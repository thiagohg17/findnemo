#!/usr/bin/env python
import math
import rospy
import cv2
import numpy as np

from geometry_msgs.msg import Point,PointStamped,Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Image
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from cv_bridge import CvBridge

rospy.init_node("search",anonymous=True)

# Sonar & Odom Class

class PositionHandler():
    def __init__(self):
        rospy.Subscriber("/sonar_data", PointStamped, self.callbackSonar)
        rospy.Subscriber("/odom", Odometry, self.callbackOdom)
        self.pub = rospy.Publisher("/cmd_vel", Twist,queue_size=10)
        self.rate = rospy.Rate(10)

        self.marlin = np.array([5,7,0.23])
        self.nemo = np.array([0,0])
        self.delta = np.array([0,0])
        self.R = 0
        self.vel = np.array([0,0])

        self.yaw = 0 # no need for roll and pitch
        self.targetAngle = 0
        self.targetAngle2 = 0
        self.anglediff = 0
    
    ### FUNCTIONS TO WORK WITH ANGLES AND VECTORS ###

    # If the angle is negative turn it in their positive counterpart
    angleRotate = lambda self,angle: (angle + 2*math.pi) if angle < 0 else angle

    sizeOfVector = lambda self,vector: (vector**2).sum()**0.5

    # Given the distances in x and y between two points returns the angle
    def getVectorAngle(self,x,y):
        if y == 0:
            return 0
        angle = math.atan(-x/y)
        if y > 0:
            angle += math.pi if x > 0 else -math.pi
        return angle
    
    # Uses a rotation matrix to rotate a vector
    def rotateVector(self,vector,angle):
        rMatrix = np.array([[math.cos(angle),-math.sin(angle)],[math.sin(angle),math.cos(angle)]])
        return np.matmul(rMatrix,vector)
        
    ###################################################

    def callbackSonar(self,data):
        self.delta[0] = data.point.x
        self.delta[1] = data.point.y
        self.nemo[0] = self.marlin[0] - self.delta[0]
        self.nemo[1] = self.marlin[1] - self.delta[1]
        self.R = self.sizeOfVector(self.delta)
        self.targetAngle = self.getVectorAngle(self.delta[0],self.delta[1])

    def callbackOdom(self,data):
        self.marlin[0] = data.pose.pose.position.x
        self.marlin[1] = data.pose.pose.position.y

        q = data.pose.pose.orientation
        roll,pitch,yaw = euler_from_quaternion([q.x,q.y,q.z,q.w])
        self.yaw = yaw

    def findingNemo(self):
        v = Twist()
        self.anglediff = self.targetAngle - self.yaw

        VectorR = self.rotateVector(np.array([0,self.R]),self.yaw+math.pi)
        Direction = self.delta
        RotationVector = np.cross(VectorR,Direction)

        RotationVector = 1 if RotationVector == 0 else abs(RotationVector)/RotationVector


        if abs(self.anglediff) > 0.3:
            v.angular.z = 5*RotationVector
        if self.R > 2 and abs(self.anglediff) < 0.6:
            v.linear.y = 2
        return v
    
    def searchThroughSonar(self):
        while not rospy.is_shutdown():
            v = self.avoidColision() if self.checkColision() else self.findingNemo()
            self.pub.publish(v)
            self.rate.sleep()
"""
### Camera Attempt

class CameraHandler:
    def __init__(self):
        self.img = np.zeros((1000, 1000))
        self.nemo = self.img
        self.bridge = CvBridge()
        rospy.Subscriber("camera/image_raw", Image, self.callback)
        self.pubNemo = rospy.Publisher("camera/nemo_mask", Image,queue_size=10)
        self.pubVel = rospy.Publisher("/cmd_vel", Twist, queue_size=10)
        rospy.wait_for_message("camera/image_raw", Image)

    def callback(self, msg):
        try:
            self.img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError, e:
            print("Frame Dropped: ", e)
        
        self.nemo = self.nemoFilter(self.img)
        self.find()
        self.publishMask()
    
    def publishMask(self):
        nemoMask = self.bridge.cv2_to_imgmsg(self.nemo, "bgr8")
        self.pubNemo.publish(nemoMask)
    
    def nemoFilter(self,frame):
        light_red = (0,255,0)
        dark_red = (255,255,255)
        
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(frame, light_red, dark_red)
        frame = cv2.bitwise_and(frame, frame, mask=mask)
        frame = cv2.cvtColor(frame,cv2.COLOR_HSV2BGR)
        
        return frame
      
    def find(self):
        global state
        img_blur = cv2.bilateralFilter(self.nemo, d = 7, sigmaSpace = 75, sigmaColor =75)
        img_gray = cv2.cvtColor(img_blur, cv2.COLOR_RGB2GRAY)
        a = img_gray.max() 
        _, thresh = cv2.threshold(img_gray, a/2+60, a,cv2.THRESH_BINARY_INV)
        image, contours, hierarchy = cv2.findContours(image = thresh, mode = cv2.RETR_TREE, method = cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key = cv2.contourArea, reverse = True)
        img_copy = self.nemo.copy()
        final = cv2.drawContours(img_copy, contours, contourIdx = -1, color = (255, 0, 0), thickness = 2)
        try:
            c_0 = contours[0]
            M = cv2.moments(c_0)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            state = 'camera'
        except IndexError:
            center = (639,359)
            state = 'sonar'

        if center == (639,359):
            state = 'sonar'

        if state == 'camera':
            M = cv2.moments(c_0)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            #cv2.cilinder(frame, (int(x), int(y)), int(radius),(0, 0, 0), 2)
            height,width,channels = self.nemo.shape
            err = width/2 - center[0]
            print(center)
        
            v = Twist()
            if err > 20:
                print('rot z')
                v.angular.z = 5 if err > width/2 else -5
            else:
                print('vel y')
                v.linear.y = 2
            self.pubVel.publish(v)
           
    def fetchImg(self):
        print('fecthed')
        return self.img
"""

def controler():
    poshandler = PositionHandler()
    poshandler.searchThroughSonar()

if __name__ == '__main__':
    try:
        controler()
    except KeyboardInterrupt:
        pass